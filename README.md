# MaterialDDG
A Material Design-inspired UserCSS theme for DuckDuckGo

[![Version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https://codeberg.org/api/v1/repos/reizumi/MaterialDDG/tags&query=$[0].name)](https://codeberg.org/reizumi/MaterialDDG/tags) [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://codeberg.org/reizumi/MaterialDDG/raw/branch/master/material-ddg.user.css)

![Screenshot](https://codeberg.org/reizumi/MaterialDDG/raw/branch/master/images/screenshot.png)

## Features
- A UserCSS theme inspired by Google's Material Design and their search engine
- Comes with built-in color themes accessible through the Stylus menu
- Customizable color options (if 'Custom' color theme is selected)
- Customizable user interface options (fonts, buttons, favicons, etc.)

## Installation
Install [Stylus](https://add0n.com/stylus.html) and this theme by clicking the button below.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://codeberg.org/reizumi/MaterialDDG/raw/branch/master/material-ddg.user.css)
